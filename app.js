var express 		= require('express'),
	app 			= express();
var path 			= require('path');
global.feedRoute	= path.resolve(__dirname);
var port 			= process.env.PORT || 1337;

// Initialize custom routes -
// var authRouter 	= require(feedRoute + '/routes/authRouter');
var feedRouter 	= require(feedRoute + '/routes/feedRouter');

// app.use('/users', authRouter);
app.use('/feeds', feedRouter);

app.listen(port);

console.log("Feeder listening @ " + port);