var Promise 		= require("bluebird");
var validate 		= require('validator');
var sanitize 		= require('sanitizer');
var parser 			= require('body-parser');
var feedRouter 		= require('express-promise-router')();

// Custom Libraries
var security 	= require(feedRoute + '/lib/security');
var dbHandle 	= require(feedRoute + '/lib/database');

/*				   ENABLE PARSING POST REQUESTS
---------------------------------------------*/
feedRouter.use(parser.urlencoded({ extended : true }));
feedRouter.use(parser.json());

/*				       BOOKSHELF INITIALIZATION!
---------------------------------------------*/
var Bookshelf 		= dbHandle;

/*				      FEED MODEL FOR BOOKSHELF!
---------------------------------------------*/
var Feeds = Bookshelf.Model.extend({
	tableName: 'tbl_feeds'
});

/*				       FEED ROUTES - BOOKSHELF!
---------------------------------------------*/
feedRouter.route('/list')
	.get(function (req, res) {
		Feeds.query({
			offset: 0,
			limit: 5		// By default, limit how many feeds can user view, if not specified!
		}).fetchAll({require:true})
		.then(function (feed) {
			// Show them the latest 5 public feeds!
			if(!feed){
				res.status(404).json({ error:true, data: {message:"Couldn't find the feed!"} });
			}else{
				res.json({error:false, data: feed.toJSON()});
			}
		}).catch(Feeds.NotFoundError, function () {
			res.json({error:true, data: {message: "There are currently no feeds in the Feed Engine!"}});
		}).catch(function () {
			res.json({error:true, data: {message: "No feeds were found in the database!"}});
		});
	});

feedRouter.route('/list/:feedID')
	.get(function (req, res) {
		new Feeds({f_id: validate.toInt(req.params.feedID)})
		.fetch({require:true})
		.then(function (feed) {
			res.json(feed.toJSON());
		}).catch(Feeds.NotFoundError, function () {
			res.json({error:true, data: {message: "No feed was found in our database with the ID you provided - " + req.params.feedID + "."}});
		}).catch(function () {
			res.json({error:true, data: {message: "No feeds were found in the database!"}});
		});
	});

feedRouter.route('/list/:offset/:limit')
	.get(function (req, res) {
		return Promise.try(function () {
			Feeds.query({
				offset: validate.toInt(req.params.offset),
				limit: validate.toInt(req.params.limit)
			}).query('orderBy', 'f_id', 'DESC')
			.fetchAll({require: true})
			.then(function (feeds) {
				// Return specified amount of latest feeds!
				if(!feeds){
					res.json({error:true, data: {message:"Something went wrong with the offsets & limits!"}});
				}else{
					res.json({error:false, data: feeds.toJSON()});
				}
			}).catch(Feeds.NotFoundError, function () {
				res.json({error:true, data: {message: "No feeds found between " + req.params.offset + " and " + req.params.limit + "."}});
			}).catch(function () {
				res.json({error:true, data: {message: "No feeds were found in the database!"}});
			});
		});
	});

feedRouter.route('/list/:sortAs/:offset/:limit')
	.get(function (req, res) {
		return Promise.try(function () {
			return (validate.isAlpha(req.params.sortAs) && validate.isInt(req.params.offset) && validate.isInt(req.params.limit, {min:1, max:20}));
		}).then(function (meetsCriteria) {
			if(meetsCriteria === true){
				Feeds.query({ offset: validate.toInt(req.params.offset), limit: validate.toInt(req.params.limit)})
				.query('orderBy', 'f_id', req.params.sortAs + "")
				.fetchAll({require:true})
				.then(function (feeds) {
					if(!feeds){
						res.status(404).json({error:true, data: {message:"Something is wrong with the parameters you provided!"}});
					}else{
						res.json({error:false, data: feeds.toJSON()});
					}
				}).catch(Feeds.NotFoundError, function () {
					res.json({error:true, data: {message: "No feeds found between " + req.params.offset + " and " + req.params.limit + "."}});
				});
			}else{
				res.json({error:true, data: {message:"The search paramters you provided does not meet the standard set by our feed engine!"}});
			}
		});
	});

/*				      ADD NEW FEED - BOOKSHELF!
---------------------------------------------*/
feedRouter.use('/new', function (req, res, next) {
	return Promise.try(function () {
		return (
				validate.isAlphanumeric(req.body.title + ""),
				validate.isAlphanumeric(req.body.content + ""),
				validate.isAlphanumeric(req.body.submittedBy + ""),
				validate.isInt(req.body.timestamp + "", {min:0, max:0}),
				validate.isInt(req.body.votes + "", {min:0, max:0})
			);
	}).then(function (validParameters) {
		if(validParameters == true){
			req.body.timestamp = new Date().getTime()
			// req.body.decodedTimeStamp = new Date(req.body.timeStamp).toString();
			req.body.votes = JSON.stringify({ "likes": 0, "dislikes": 0 });
			next();
		}else{
			res.json({error:true, data:{message:"The values you provided does not meet the criteria set by the Feed Engine."}});
		}
	});
});

feedRouter.route('/new')
	.post(function (req, res) {
		// Now that we have checked and verified that the passed values are VALID, let's add it to the database!
		// Let's clear up the values and create proper array sort that matches our feeds table struct.
		new Feeds({ 
			f_title: req.body.title,
			f_content: req.body.content,
			f_submittedBy: req.body.submittedBy,
			f_timestamp: req.body.timestamp,
			f_votes: req.body.votes
		}).save().then(function (feed) {
			res.json({error:false, data:{message: "Your feed has been successfully posted and your FID : " + feed.get('id')}});
		});
	});

/*				      	  DELETE EXISTING FEED!
---------------------------------------------*/
feedRouter.use('/delete/:feedID', function (req, res, next) {
	return Promise.try(function () {
		return validate.isInt(req.params.feedID);
	}).then(function (isValidID) {
		(isValidID == true) ? next() : res.json({error:true, data:{message: "For the feed you wanted to delete, you passed an ID that does not meet Feed Engine's criteria."}});
	});
});

feedRouter.route('/delete/:feedID')
	.get(function (req, res) {
		Feeds.where({f_id: validate.toInt(req.params.feedID)})
		.destroy({require:true})
		.then(function (feed){
			res.json({error:false, data:{message: "Feed has been successfully deleted!"}});
		}).catch(Feeds.NoRowsDeletedError, function() {
			res.json({error:true, data: {message: "Feed with ID - " + req.params.feedID + " is not deleted because it does not exist anymore!"}});
		}).catch(Feeds.NotFoundError, function () {
			res.json({error:true, data: {message: "No feed with the FID - " + req.params.feedID + " found in our database!"}});
		});
	});

module.exports = feedRouter;