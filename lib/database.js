var Promise 		= require('bluebird');

/*				   ENABLE PARSING POST REQUESTS
---------------------------------------------*/
var config 			= require(feedRoute + '/lib/config');

// Using pool instead of 1-1 connection for better & faster processing!
var knex = require('knex')({
	client: 'mysql',
	connection: {
		host     : config.mhost,
		user     : config.muser,
		password : config.mpass,
		database : config.mdb
	},
	pool: {
		min: 0,
		max: config.mconns
	}
});

var Bookshelf = require('bookshelf')(knex);

module.exports = Bookshelf;